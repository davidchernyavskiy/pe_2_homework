// циклы нужны для того что бы выполнять разные действия заданное количесто раз, и в разном виде

// вместо этого: 
// console.log("1");
// console.log("2");
// console.log("3");
// console.log("4");
// console.log("5");

// это:
// for (let i = 1; i <=5; i++) {
//     console.log(i)
// }



const userNumber = prompt("Введите число");

if (userNumber >= 5) {
    for (let i = 0; i <= userNumber; i++) {
        if (i % 5 == 0) {
            console.log(i);
        }
    }
} else {
    console.log("Sorry, no numbers");
}

