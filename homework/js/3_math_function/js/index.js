// функции нужны для разделения программы на разные функции,
// иначе говоря для порядка, и на деление программы на разные ее состовляющие.

const firstNumber = prompt("введите число");
const operation = prompt("введите операцию");
const secondNumber = prompt("введите второе число");

function math(firstNumber, secondNumber, operation) {
    if (operation === "-") {
        return firstNumber - secondNumber;
    } else if (operation === "+") {
        return +firstNumber + +secondNumber;
    } else if (operation === "*") {
        return firstNumber * secondNumber;
    } else if (operation === "/") {
        return firstNumber / secondNumber;
    }
}

console.log("результат " + math(firstNumber, secondNumber, operation));
